import entity.Dvd;
import service.MessageService;
import sun.plugin2.message.Message;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;

public class ServletAddDvd extends javax.servlet.http.HttpServlet {
    private MessageService messageService;

    public ServletAddDvd() throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, IOException {
        messageService = new MessageService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        String year = request.getParameter("year");
        String price = request.getParameter("price");
        String action = request.getParameter("action");
        String error = "";
        switch (action){
            case "Add":
                Dvd dvd = new Dvd(title, Integer.parseInt(year), Integer.parseInt(price));
                messageService.sendMessage(dvd.toString());
                break;
        }
        printPage(response, error);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        printPage(response, "");
    }

    private void printPage(HttpServletResponse response, String error) throws IOException{
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Add DVD</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("</br>");
            printDvdForm(out);
            out.println("<p><b>" + error + "</b></p>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private void printDvdForm(PrintWriter out) {
        out.println("<hr/>");
        out.println("<form method=\"post\">");
        out.println("<legend><b>Add a DVD:</b></legend>");
        out.println("Title: <input type=\"text\" name=\"title\" required/>");
        out.println("<p>");
        out.println("Year: <input type=\"number\" min=\"1900\" max=\"2200\" name=\"year\" required/>");
        out.println("<p>");
        out.println("Price: <input type=\"number\" min=\"0.0\" name=\"price\" required/>");
        out.println("<p>");
        out.println("<input type=\"submit\" class=\"button\" name=\"action\" value=\"Add\" />");
        out.println("</form>");
    }
}
