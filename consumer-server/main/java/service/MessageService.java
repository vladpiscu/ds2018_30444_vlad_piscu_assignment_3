package service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class MessageService {
    private QueueingConsumer consumer;
    private static final String EXCHANGE_NAME = "hello";

    public MessageService() throws IOException, NoSuchAlgorithmException, KeyManagementException, URISyntaxException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://fapbdvga:F73HowFGfYE3kMhLrW8XFQdUwuu3L6l_@bee.rmq.cloudamqp.com/fapbdvga");

        factory.setRequestedHeartbeat(30);
        factory.setConnectionTimeout(30000);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        consumer = new QueueingConsumer(channel);
        channel.basicConsume(queueName, true, consumer);
    }

    public String readMessage() throws Exception {
        String message;

        QueueingConsumer.Delivery delivery = consumer.nextDelivery();
        message = new String(delivery.getBody());
        System.out.println(" [x] Received '" + message + "'");

        return message;
    }
}
