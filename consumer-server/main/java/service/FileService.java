package service;

import java.io.File;
import java.io.FileWriter;

public class FileService {
    private int fileNumber;

    public FileService(){
        fileNumber = 0;
    }

    public void writeMessage(String message) {
        File file = new File("message" + fileNumber + ".txt");
        fileNumber++;
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(message);
            fileWriter.close();
        }catch(Exception e){
            System.out.println(e);
        }
    }
}
