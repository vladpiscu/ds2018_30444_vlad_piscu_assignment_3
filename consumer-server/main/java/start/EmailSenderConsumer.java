package start;

import service.FileService;
import service.MailService;
import service.MessageService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class EmailSenderConsumer {
	private static final String[] subscribers = {"vlady301@gmail.com", "piscu.vlad@gmail.com"};

	private EmailSenderConsumer() {
	}

	public static void main(String[] args) throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, IOException {
		MessageService messageService = new MessageService();

		MailService mailService = new MailService("dummyds256","Test.1234");
		String message;

		while(true) {
			try {
				message = messageService.readMessage();
				for(String subscriber : subscribers)
					mailService.sendMail(subscriber,"Dummy Mail Title",message);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
