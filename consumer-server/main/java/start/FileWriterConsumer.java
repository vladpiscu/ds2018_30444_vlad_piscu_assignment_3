package start;

import service.FileService;
import service.MailService;
import service.MessageService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class FileWriterConsumer {

    public static void main(String[] args) throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, IOException {
        MessageService messageService = new MessageService();

        FileService fileService = new FileService();
        String message;

        while(true) {
            try {
                message = messageService.readMessage();
                fileService.writeMessage(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
